import { AuthRoutingModule } from "./auth-routing.module";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { RegisterComponent } from "./register/register.component";
import { LoginComponent } from "./login/login.component";

@NgModule({
  imports: [FormsModule, AuthRoutingModule],
  exports: [LoginComponent, RegisterComponent],
  declarations: [LoginComponent, RegisterComponent],
  providers: []
})
export class AuthModule {}
