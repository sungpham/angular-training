import { AuthModule } from "./auth/auth.module";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LandingComponent } from "./landing/landing.component";
import { UserProfileComponent } from "./account/user-profile/user-profile.component";

@NgModule({
  declarations: [AppComponent, LandingComponent, UserProfileComponent],
  imports: [BrowserModule, AppRoutingModule, AuthModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
