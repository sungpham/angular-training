import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";

interface User {
  firstName: string;
  lastName: string;
  gender: string;
  age: number;
  mobile: string;
  interest: string[];
}

@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.scss"]
})
export class UserProfileComponent implements OnInit {
  user: User;

  constructor() {}

  ngOnInit() {}
  updateUserProfile(formUserProfile: NgForm) {}
}
